## [1.0.1] - 03.03.2020

* Added more Documentation
* fixed formatting

## [0.0.1] - 01.03.2020

* Added "ping" function, returns ping to web address in ms
* First Release
