# easyping

This Package Allows you to easily get the ping to an internet Address, and to get the ping in Milliseconds,
Returns -1 if the destination is not available.

## Getting Started

Import the package and use the async method 'ping' to get the ping to the Address

```dart
import 'package:easy_ping/easydartping.dart';

//to get the ping just use
String address = 'google.com';
ping(address).then(pingToAddress){
  //to something...
}

// you can also use async/await
void function() async{
  String address = 'google.com';
  double pingToAddress = ping(address);
  //...
}

```
